/*
 * Grid Messages
 *
 * This contains all the text for the Grid component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.Grid.header',
    defaultMessage: 'This is the Grid component !',
  },
});
