/*
 * Graph Messages
 *
 * This contains all the text for the Graph component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.Graph.header',
    defaultMessage: 'This is Graph container !',
  },
});
