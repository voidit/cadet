/*
 * Stats Messages
 *
 * This contains all the text for the Stats component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.Stats.header',
    defaultMessage: 'This is Stats container !',
  },
});
