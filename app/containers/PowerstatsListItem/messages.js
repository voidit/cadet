/*
 * PowerstatsListItem Messages
 *
 * This contains all the text for the PowerstatsListItem component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.PowerstatsListItem.header',
    defaultMessage: 'This is PowerstatsListItem container !',
  },
});
