/**
 *
 * Asynchronously loads the component for PowerstatsListItem
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
