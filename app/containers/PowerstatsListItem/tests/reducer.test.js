import { fromJS } from 'immutable';
import powerstatsListItemReducer from '../reducer';

describe('powerstatsListItemReducer', () => {
  it('returns the initial state', () => {
    expect(powerstatsListItemReducer(undefined, {})).toEqual(fromJS({}));
  });
});
