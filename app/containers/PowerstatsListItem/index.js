/**
 *
 * PowerstatsListItem
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectPowerstatsListItem from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

/* eslint-disable react/prefer-stateless-function */

export class PowerstatsListItem extends React.Component {
  constructor(props) {
    super(props);
    // let { intelligence, strength, speed, durability, power, combat } = props;
    // this.state = {
    //   intelligence,
    //   strength,
    //   speed,
    //   durability,
    //   power,
    //   combat,
    // };
    // this.changeOrder = this.changeOrder.bind(this);
  }
  render() {
    return (
      <div>
        <FormattedMessage {...messages.header} />
      </div>
    );
  }
}

PowerstatsListItem.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  powerstatslistitem: makeSelectPowerstatsListItem(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'powerstatsListItem', reducer });
const withSaga = injectSaga({ key: 'powerstatsListItem', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(PowerstatsListItem);
