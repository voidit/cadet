import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the powerstatsListItem state domain
 */

const selectPowerstatsListItemDomain = state =>
  state.get('powerstatsListItem', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by PowerstatsListItem
 */

const makeSelectPowerstatsListItem = () =>
  createSelector(selectPowerstatsListItemDomain, substate => substate.toJS());

export default makeSelectPowerstatsListItem;
export { selectPowerstatsListItemDomain };
