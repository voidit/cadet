import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the connections state domain
 */

const selectConnectionsDomain = state => state.get('connections', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by Connections
 */

const makeSelectConnections = () =>
  createSelector(selectConnectionsDomain, substate => substate.toJS());

export default makeSelectConnections;
export { selectConnectionsDomain };
