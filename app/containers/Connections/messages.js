/*
 * Connections Messages
 *
 * This contains all the text for the Connections component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.Connections.header',
    defaultMessage: 'This is Connections container !',
  },
});
