import { fromJS } from 'immutable';
import connectionsReducer from '../reducer';

describe('connectionsReducer', () => {
  it('returns the initial state', () => {
    expect(connectionsReducer(undefined, {})).toEqual(fromJS({}));
  });
});
