/*
 * HeroCard Messages
 *
 * This contains all the text for the HeroCard component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.HeroCard.header',
    defaultMessage: 'This is HeroCard container !',
  },
});
