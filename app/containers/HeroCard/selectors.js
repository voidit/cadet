import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the heroCard state domain
 */

const selectHeroCardDomain = state => state.get('heroCard', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by HeroCard
 */

const makeSelectHeroCard = () =>
  createSelector(selectHeroCardDomain, substate => substate.toJS());

export default makeSelectHeroCard;
export { selectHeroCardDomain };
