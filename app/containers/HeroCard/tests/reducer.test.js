import { fromJS } from 'immutable';
import heroCardReducer from '../reducer';

describe('heroCardReducer', () => {
  it('returns the initial state', () => {
    expect(heroCardReducer(undefined, {})).toEqual(fromJS({}));
  });
});
