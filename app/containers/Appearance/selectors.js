import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the appearance state domain
 */

const selectAppearanceDomain = state => state.get('appearance', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by Appearance
 */

const makeSelectAppearance = () =>
  createSelector(selectAppearanceDomain, substate => substate.toJS());

export default makeSelectAppearance;
export { selectAppearanceDomain };
