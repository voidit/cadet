import { fromJS } from 'immutable';
import appearanceReducer from '../reducer';

describe('appearanceReducer', () => {
  it('returns the initial state', () => {
    expect(appearanceReducer(undefined, {})).toEqual(fromJS({}));
  });
});
