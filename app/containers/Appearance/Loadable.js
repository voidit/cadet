/**
 *
 * Asynchronously loads the component for Appearance
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
