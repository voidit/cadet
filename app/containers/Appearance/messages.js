/*
 * Appearance Messages
 *
 * This contains all the text for the Appearance component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.Appearance.header',
    defaultMessage: 'This is Appearance container !',
  },
});
