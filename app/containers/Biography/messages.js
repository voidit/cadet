/*
 * Biography Messages
 *
 * This contains all the text for the Biography component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.Biography.header',
    defaultMessage: 'This is Biography container !',
  },
});
