import { fromJS } from 'immutable';
import heroPanelReducer from '../reducer';

describe('heroPanelReducer', () => {
  it('returns the initial state', () => {
    expect(heroPanelReducer(undefined, {})).toEqual(fromJS({}));
  });
});
