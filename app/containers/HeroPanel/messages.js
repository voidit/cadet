/*
 * HeroPanel Messages
 *
 * This contains all the text for the HeroPanel component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.HeroPanel.header',
    defaultMessage: 'This is HeroPanel container !',
  },
});
