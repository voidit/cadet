import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the heroPanel state domain
 */

const selectHeroPanelDomain = state => state.get('heroPanel', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by HeroPanel
 */

const makeSelectHeroPanel = () =>
  createSelector(selectHeroPanelDomain, substate => substate.toJS());

export default makeSelectHeroPanel;
export { selectHeroPanelDomain };
