/*
 * Hero Messages
 *
 * This contains all the text for the Hero component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.Hero.header',
    defaultMessage: 'This is Hero container !',
  },
});
