// import { take, call, put, select } from 'redux-saga/effects';

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
}

/**
 * Gets the all SuperHeroes and Villians data from SuperHero API.
 */

import { call, put, select, takeLatest } from 'redux-saga/effects';
import { LOAD_HERO } from 'containers/Hero/constants';
import { heroLoaded, heroLoadingError } from 'containers/Hero/actions';

import request from 'utils/request';
import { makeSelectHero } from 'containers/HomePage/selectors';

/**
 * Github repos request/response handler
 */
export function* getRepos() {
  // Select username from store
  const username = yield select(makeSelectUsername());
  const requestURL = `https://api.github.com/users/${username}/repos?type=all&sort=updated`;

  try {
    // Call our request helper (see 'utils/request')
    const repos = yield call(request, requestURL);
    yield put(reposLoaded(repos, username));
  } catch (err) {
    yield put(repoLoadingError(err));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* githubData() {
  // Watches for LOAD_HERO actions and calls getRepos when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount
  yield takeLatest(LOAD_HERO, getRepos);
}
