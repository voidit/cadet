import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the hero state domain
 */

const selectHeroDomain = state => state.get('hero', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by Hero
 */

const makeSelectHero = () =>
  createSelector(selectHeroDomain, substate => substate.toJS());

export default makeSelectHero;
export { selectHeroDomain };
