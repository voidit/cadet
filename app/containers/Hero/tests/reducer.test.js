import { fromJS } from 'immutable';
import heroReducer from '../reducer';

describe('heroReducer', () => {
  it('returns the initial state', () => {
    expect(heroReducer(undefined, {})).toEqual(fromJS({}));
  });
});
