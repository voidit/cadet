/*
 *
 * Hero reducer
 *
 */

import { fromJS } from 'immutable';
import { LOAD_HERO } from './constants';

export const initialState = fromJS({});

function heroReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_HERO:
      return state;
    default:
      return state;
  }
}

export default heroReducer;
