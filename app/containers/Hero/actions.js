/*
 *
 * Hero actions
 *
 */

import { LOAD_HERO } from './constants';

export function defaultAction() {
  return {
    type: LOAD_HERO,
  };
}
